import "./App.css";
import CalendarComponent from "./Calendar.js";

function App() {
  return (
    <div>
      <CalendarComponent />
    </div>
  );
}

export default App;
