import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import React, { useState } from "react";

const CalendarComponent = () => {
  return <Calendar calendarType="US" />;
};

export default CalendarComponent;
